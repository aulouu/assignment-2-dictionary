global _start

%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define BUFFER_SIZE 255
%define POINTER_SIZE 8
%define EXIT_CODE_ERROR 1

section .bss
	buf: resb (BUFFER_SIZE + 1)

section .rodata
	not_found_error: db "Value not found", 0
	length_error: db "Input value too long (not more than 255 chars)", 0 

section .text
_start:
    mov rdi, buf
    mov rsi, BUFFER_SIZE
    call read_word
    test rax, rax
    je .not_length
    push rdx
    mov rdi, rax           
    mov rsi, last
    call find_word
    test rax, rax
    je .not_found
    pop rdx 
    mov rdi, rax
    add rdi, POINTER_SIZE
    add rdi, rdx
    inc rdi
    call print_string
    call print_newline
    xor rdi, rdi
    call exit
.not_found:
    mov rdi, not_found_error
    jmp .end
.not_length:
    mov rdi, length_error
.end:
    call print_error
    call print_newline
    mov rdi, EXIT_CODE_ERROR
    call exit
    
