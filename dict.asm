global find_word

%include "lib.inc"

%define POINTER_SIZE 8

section .text

; Ищет вхождения ключа в словаре
; Если подходящее вхождение найдено, возвращает адрес начала вхождения в словарь (не значения), иначе 0
; rdi = указатель на нуль-терминированную строку
; rsi = указатель на начало словаря
find_word:
	push r12
	push r13
	mov r12, rdi
	mov r13, rsi
.cycle:
	mov rdi, r12
	add r13, POINTER_SIZE
	call string_equals
	test rax, rax
	jne .found
	mov rsi, [r13] 
	test r13, r13
	je .not_found
	jmp .cycle
.found:
	mov rax, r13
	jmp .end 
.not_found: 
	xor rax, rax
.end:
	pop r13
	pop r12
	ret
