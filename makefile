PYC = python3
ASM = nasm
ELF = -felf64
LD = ld
RM = rm

main.o: main.asm lib.inc words.inc dict.inc
dict.o: dict.asm lib.inc
words.inc: colon.inc

%.o: %.asm  
	$(ASM) $(ELF) -o $@ $<

main: lib.o main.o dict.o
	$(LD) -o main main.o lib.o dict.o

clean: 
	$(RM) -f *.o test*

test: main
	$(PYC) test.py

.PHONY: clean test
