global exit;
global string_length;
global print_string;
global print_newline;
global print_char;
global print_int;
global print_uint;
global string_equals;
global read_char;
global read_word;
global parse_uint;
global parse_int;
global string_copy;
global print_error;


%define SYS_EXIT 60
%define SYS_READ 0
%define SYS_WRITE 1

%define STD_IN 0
%define STD_OUT 1
%define STD_ERR 2

%define SPACE 0x20
%define TAB 0x9
%define LINE 0xA


section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall 


; Принимает указатель на нуль-терминированную строку, возвращает её длину
; rdi = адрес строки
; rax = длина строки
string_length:
    xor rax, rax
.cycle:
    cmp byte[rdi + rax], 0
    je .end
    inc rax
    jmp .cycle
.end:    
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; rdi = адрес строки
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, SYS_WRITE
    mov rdi, STD_OUT
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, LINE


; Принимает код символа и выводит его в stdout
; rdi = код символа
print_char:
    push rdi
    mov rdx, 1                  ; длина = 1, один символ     
    mov rsi, rsp                ; загружаем указатель на вершину стека, чтобы передать его как адрес буфера для вывода
    mov rax, SYS_WRITE
    mov rdi, STD_OUT
    syscall
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате
; rdi = нужное число 
print_int:
    test rdi, rdi
    jnl print_uint              ; если >= 0, то переход
    push rdi
    mov rdi, '-'                ; выводим минус
    call print_char
    pop rdi
    neg rdi


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, 10                  ; основание сс
    dec rsp
    mov byte[rsp], 0            ; кладем 0 на вершину стека для нуль-терминированного print_string
    mov r9, 1                   ; кол-во байт
.cycle:
    inc r9
    xor rdx, rdx
    div r8                      ; делим, результат в регистор rax, остаток в регистор rdx
    add rdx, '0'                ; перевод в ASCII код
    dec rsp
    mov byte[rsp], dl           ; кладем на вершину стека результат деления
    test rax, rax
    jnz .cycle
.print:
    mov rdi, rsp
    push r9
    call print_string
    pop r9
    add rsp, r9
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi = указатель на первую строку
; rsi = указатель на вторую строку
string_equals:
    xor rax, rax
.cycle:
    mov al, byte[rdi]
    cmp al, byte[rsi]
    jnz .false
    inc rdi
    inc rsi
    test al, al
    jnz .cycle
    mov rax, 1
    ret
.false:
    xor rax, rax
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push 0
    mov rdx, 1                  ; длина = 1, один символ
    mov rsi, rsp                ; загружаем указатель на вершину стека, чтобы передать его как адрес буфера для чтения
    mov rax, SYS_READ
    mov rdi, STD_IN
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; принимает rdi = адрес начала буфера, rsi = размер буфера
; возвращает rax = адрес буфера, rdx = длина слова
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14
.cycle:
    call read_char
    test rax, rax
    jz .okey
    cmp r14, r13
    jnl .err
    cmp rax, SPACE
    jz .skip
    cmp rax, TAB
    jz .skip
    cmp rax, LINE
    jz .skip
    mov [r12 + r14], al
    inc r14
    jmp .cycle
.skip:
    test r14, r14
    jz .cycle
.okey:
    mov byte[r12 + r14], 0
    mov rax, r12
    mov rdx, r14
    jmp .exit
.err:
    xor rax, rax
.exit:
    pop r14
    pop r13
    pop r12
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor r8, r8                  ; длина
    mov r9, 10                  ; основание сс
    xor r10, r10
.cycle:
    mov r10b, byte[rdi + r8]
    cmp r10b, 0                 ; если == 0, то переход
    je .end
    cmp r10b, '0'               ; проверка на цифру, если < 0, то переход
    jb .end
    cmp r10b, '9'               ; проверка на цифру, если > 9, то переход
    ja .end
    mul r9                      ; добавляем новую цифру
    sub r10b, '0'
    add rax, r10
    inc r8
    jmp .cycle
.end:
    mov rdx, r8                 ; в rdx длина числа
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '-'
    jz .neg                     ; если < 0, то переход
    jmp parse_uint
.neg:
    inc rdi
    push rdi
    call parse_uint
    pop rdi
    neg rax
    inc rdx
    ret  


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi = указатель на строку
; rsi = указатель на буфер
; rdx = длина буфера
string_copy:
    xor rax, rax                ; длина строки         
.cycle:
    cmp rdx, rax
    jz .nolenght
    mov r8b, byte[rdi + rax]
    mov byte [rsi + rax], r8b
    inc rax
    test r8b, r8b
    jnz .cycle
    ret
.nolenght:
    xor rax, rax
    ret


; Принимает указатель на строку с сообщением об ошибки, выводит её в stderr
print_error:
    push rdi
	call string_length
	pop rsi
	mov rdx, rax                ; длина строки
	mov rax, SYS_WRITE
	mov rdi, STD_ERR
	syscall
	ret