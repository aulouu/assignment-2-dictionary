%define last 0

%macro colon 2
	%ifid %2
		%2: dq last
		%define last %2
	%else
		%error "The second value is not an id"
	%endif
	%ifstr %1
		db %1, 0
	%else
		%error "The first value is not a string"
	%endif
%endmacro