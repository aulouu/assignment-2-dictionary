from subprocess import Popen, PIPE
import os

inputs = ["a", "first", "second", "third", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"]
outputs = ["", "I", "love", "you", ""]
errors = ["Value not found", "", "", "", "Input value too long (not more than 255 chars)"]

if os.path.exists("./main"):
    print("Running tests....")

    for i in range(len(inputs)):
        p = Popen(["./main"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        data = p.communicate(inputs[i].encode())
        if data[0].decode().strip() == outputs[i] and data[1].decode().strip() == errors[i]:
            print("Test " + str(i+1) + " passed")
        else:
            print("Test " + str(i+1) + ' failed. {Output: "' + data[0].decode().strip() + '", Error: "' + data[1].decode().strip() + '"} {Need output: "' + outputs[i] + '", Need error: "' + errors[i] + '"}')

else:
    print("Executable file does not exist")